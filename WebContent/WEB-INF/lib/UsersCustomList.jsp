<%@ page language="java" import="java.util.*"   pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>有订单的用户</title>
	
  </head>
  
  <body>
    	<table>
    		<thead>
    			<tr>
    				<th>用户名</th>
    				<th>订单号</th>
    				<th>创建时间</th>
    				<th>操作</th>
    			</tr>
    		</thead>
    		<tbody>
    			<c:forEach items="${UsersCustomList}" var ="uscl">
    				<tr>
	    				<td>${uscl.username}</td>
	    				<td>${uscl.number}</td>
	    				<td>
	    					 <fmt:formatDate pattern="yyyy-MM-dd"  value="${uscl.createtime}" />
	    				 </td>
	    				<td><a href="<%=basePath%>queryOrdersDetailByOrdersId.action?orders_id=${uscl.orders_id}">订单详情</a></td>
	    			</tr>
    			</c:forEach>
    		</tbody>
    	</table>
  </body>
</html>
