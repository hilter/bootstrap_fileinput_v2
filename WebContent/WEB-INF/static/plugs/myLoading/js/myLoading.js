//showLoding，showToast这两者结合使用
//显示加载动画
function showLoding(){
	$("body").append("<div class='mask'></div>");
	$(".mask").css("height",$(window).height());
	$("body").css({"overflow-y":"hidden"});
}

//显示Toast
function showToast(str){
	$(".mask").append(
		"<div class='toast'>"+
			"<div class='toast_title'>提示</div>"+
			"<div class='toast_content'>"+str+"</div>"+
		"</div>"
	)
}
//显示Confirm
function showConfirm(str){
	showLoding();
	$(".mask").append(
		"<div class='toast'>"+
			"<div class='toast_title'>提示</div>"+
			"<div class='toast_content'>"+str+"</div>"+
			"<div class='toast_footer'>"+
				"<button class='f_sure' onclick='clickSure()'>确认</button>"+
				"<button class='f_no' onclick='clickNo()'>取消</button>"+
			"</div>"+
		"</div>"
	)
}
//隐藏当前正在显示Toast或者Confirm
function hideConfirmOrToast(){
	$(".toast").remove();
}










//隐藏全部
function hideLoding(){
	//删除遮罩
	$(".mask").remove();
	//隐藏滚动条
	$("body").css({"overflow-y":"auto"});
}

