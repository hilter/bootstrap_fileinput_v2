<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
	<head>
		<meta charset="utf-8">
		<title>图片上传</title>
		<!-- jq -->
		<script type="text/javascript" src="${basePath}static/plugs/jquery-3.1.1.min.js"></script>
		
		<!-- bootstrap -->
		<link rel="stylesheet" href="${basePath}static/plugs/bootstrap/css/bootstrap.min.css">
		<script type="text/javascript" src="${basePath}static/plugs/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- 图片上传即使预览插件 -->
		<link rel="stylesheet" href="${basePath}static/plugs/bootstrap-fileinput/css/fileinput.min.css">
		<script type="text/javascript" src="${basePath}static/plugs/bootstrap-fileinput/js/fileinput.min.js"></script>
		
		<style>
			.container{padding-top:60px}
			.pic{width:100px;}
			td {vertical-align: middle!important;}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
			<button  class="btn btn-default col-sm-2 pull-right add">新增</button>
			<table class="table table-striped table-bordered">
				<caption>文章列表</caption>
			<thead>
				<tr>
					 <th>id</th>
					 <th>描述</th>
					 <th>缩率图</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${artall}">
					<tr>
						<td>${item.id}</td>
						<td>${item.describ}</td>
						<td>
							<img src="${basePath}static/upload/${item.url}" class="pic"/>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>	
			</div>
		</div>
		<script>
			$(".add").click(function(){
				location.href="${basePath}goAdd";
			})
		</script>
	</body>
</html>
