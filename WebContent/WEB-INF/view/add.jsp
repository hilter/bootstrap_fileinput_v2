<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
	<head>
		<meta charset="utf-8">
		<title>图片上传</title>
		<!-- jq -->
		<script type="text/javascript" src="<%=basePath%>static/plugs/jquery-3.1.1.min.js"></script>
		
		<!-- bootstrap -->
		<link rel="stylesheet" href="<%=basePath%>static/plugs/bootstrap/css/bootstrap.min.css">
		<script type="text/javascript" src="<%=basePath%>static/plugs/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- 图片上传即使预览插件 -->
		<link rel="stylesheet" href="<%=basePath%>static/plugs/bootstrap-fileinput/css/fileinput.min.css">
		<script type="text/javascript" src="<%=basePath%>static/plugs/bootstrap-fileinput/js/fileinput.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>static/plugs/bootstrap-fileinput/js/locales/zh.js"></script>
		
		<style>
			.container{padding-top:60px}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<form class="form-horizontal"  role="form" method="post"  action="<%=basePath%>addArticle"  enctype="multipart/form-data" >
						<div class="form-group">
							<label class="col-sm-2 control-label">描述</label>
							<div class="col-sm-10">
								<input type="text" name="describ" class="col-sm-10 form-control"   placeholder="请描述">
							</div>
						</div>
					    <div class="form-group">
							<label class="col-sm-2 control-label">缩略图</label>
							<div class="col-sm-10">
								<input type="file" name="myfile" class="col-sm-10 myfile" value=""/>
								<input type="hidden" name="url" value="">
							</div>
						</div>
						<button type="submit" class="btn btn-default col-sm-2 col-sm-offset-4">提交</button>
					</form>
				</div>
			</div>
		</div>
	
		<script>
			$(".myfile").fileinput({
				uploadUrl:"<%=basePath%>uploadFile",//上传的地址
				uploadAsync:true, //默认异步上传
				showUpload: false, //是否显示上传按钮,跟随文本框的那个
				showRemove : false, //显示移除按钮,跟随文本框的那个
				showCaption: true,//是否显示标题,就是那个文本框
				showPreview : true, //是否显示预览,不写默认为true
				dropZoneEnabled: false,//是否显示拖拽区域，默认不写为true，但是会占用很大区域
				//minImageWidth: 50, //图片的最小宽度
				//minImageHeight: 50,//图片的最小高度
				//maxImageWidth: 1000,//图片的最大宽度
				//maxImageHeight: 1000,//图片的最大高度
				//maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
				//minFileCount: 0,
				 maxFileCount: 1, //表示允许同时上传的最大文件个数
				 enctype: 'multipart/form-data',
				 validateInitialCount:true,
				 previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
				 msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
				 allowedFileTypes: ['image'],//配置允许文件上传的类型
				 allowedPreviewTypes : [ 'image' ],//配置所有的被预览文件类型
				 allowedPreviewMimeTypes : [ 'jpg', 'png', 'gif' ],//控制被预览的所有mime类型
				 language : 'zh'
			})
			//异步上传返回结果处理
			$('.myfile').on('fileerror', function(event, data, msg) {
				console.log("fileerror");
				console.log(data);
			});
			//异步上传返回结果处理
			$(".myfile").on("fileuploaded", function (event, data, previewId, index) {
				console.log("fileuploaded");
				$("input[name='url']").val(data.response.url);

			});

			//同步上传错误处理
			$('.myfile').on('filebatchuploaderror', function(event, data, msg) {
				console.log("filebatchuploaderror");
				console.log(data);
			});
			
			//同步上传返回结果处理
			$(".myfile").on("filebatchuploadsuccess", function (event, data, previewId, index) {
				console.log("filebatchuploadsuccess");
				console.log(data);
			});

			//上传前
			$('.myfile').on('filepreupload', function(event, data, previewId, index) {
				console.log("filepreupload");
			});		
		</script>
	</body>
</html>
