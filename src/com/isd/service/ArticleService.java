package com.isd.service;

import java.util.List;
import com.isd.pojo.Article;

public interface ArticleService {
	int insert(Article record);
	List<Article> selectAll();
}
