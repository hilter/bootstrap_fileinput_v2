package com.isd.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.isd.mapper.ArticleMapper;
import com.isd.pojo.Article;
import com.isd.pojo.ArticleExample;
import com.isd.pojo.ArticleExample.Criteria;
import com.isd.service.ArticleService;

public class ArticleServiceImp implements ArticleService {
	@Autowired
	private ArticleMapper articleMapper;
	public int insert(Article record) {
		return articleMapper.insert(record);
	}
	
	public List<Article> selectAll() {
		ArticleExample example=new ArticleExample();
		Criteria criteria=example.createCriteria();
		criteria.getAllCriteria();
		return articleMapper.selectByExample(example);
	}
	
}
