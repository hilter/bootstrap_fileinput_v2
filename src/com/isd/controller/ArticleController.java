package com.isd.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.isd.pojo.Article;
import com.isd.service.ArticleService;

@Controller
public class ArticleController {
		//新增文章保存
		@Autowired
		private ArticleService articleService;
	
		//新增文章页面跳转
		@RequestMapping("goAdd")
		public String goAdd(){
			return "add";
		}
		//uploadFile
		@ResponseBody
		@RequestMapping("uploadFile")
		public  Map<String,Object> uploadFile(HttpSession session,MultipartFile myfile) throws IllegalStateException, IOException{
			//原始名称  
	        String oldFileName = myfile.getOriginalFilename(); //获取上传文件的原名 
	        //存储图片的物理路径  
	        String file_path = session.getServletContext().getRealPath("WEB-INF/static/upload");
	       //上传图片  
	        if(myfile!=null && oldFileName!=null && oldFileName.length()>0){
		        //新的图片名称  
		        String newFileName = UUID.randomUUID() + oldFileName.substring(oldFileName.lastIndexOf("."));  
		        //新图片  
		        File newFile = new File(file_path+"/"+newFileName);  
		        //将内存中的数据写入磁盘  
		        myfile.transferTo(newFile);
		        //将新图片名称返回到前端
		        Map<String,Object> map=new HashMap<String,Object>();
		        map.put("success", "成功啦");
		        map.put("url",newFileName);
		        return  map;
	        }else{
	        	Map<String,Object> map=new HashMap<String,Object>();
		        map.put("error","图片不合法");
	        	return map;
	        }
		}
	
	   //新增文章保存
		@RequestMapping("addArticle")
		public void addArticle(HttpSession session,HttpServletResponse response,Article record,MultipartFile image) throws IllegalStateException, IOException {
			//如果不传图片，那么则用默认的图片
			if(record.getUrl()==null||record.getUrl().equals("")){
				record.setUrl("default.png");
			}
			int i=articleService.insert(record);
	        checkUpIsOk(i,response);
		}
		
		//用于判断插入是否成功
		public void checkUpIsOk(int i,HttpServletResponse response) throws IOException{
			response.setHeader("content-type", "text/html;charset=UTF-8");
	        response.setCharacterEncoding("UTF-8");
	        PrintWriter out = response.getWriter();//获取PrintWriter输出流
			if(i==0){
	        	out.write("插入失败");
	            out.write("<script>setTimeout(function(){"+
	            		"history.go(-1);"+ 
	            "},500) </script>");
	            out.close();
	        }else{
	        	out.write("插入成功");
	            out.write("<script>setTimeout(function(){"+
	            		"location.href='goList'"+ 
	            "},500) </script>");
	            out.close();
	        }
		}
		
		//文章列表页面跳转
		@RequestMapping("goList")
		public ModelAndView goList(){
			List<Article> artall=articleService.selectAll();
			System.out.println(artall.size());
			ModelAndView mv=new ModelAndView();
			mv.addObject("artall",artall);
			mv.setViewName("list");
			return mv;
		}
}
